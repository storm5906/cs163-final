﻿/*
	Student 1 Name:   Nguyễn Lê Bảo     -	   1351051    -    nlbao@apcs.vn
	Student 2 Name:   Trương Minh Bảo   -	   1351052    -    tmbao@apcs.vn
	Course: CS163
	Program #: Final Project - Mini Search Engine
	Date: 27/06/2014
	File: Trie.h
*/

#pragma once
#include <cstring>
#include <bitset>
#include <vector>

using namespace std;

#define CHAR_RANGE 128 //The range of characters in decimal
const int FILE_LIMIT = 301;

// convert a character to an integer, use for Trie indexing
int charToNum(char x) {
	if ('0' <= x && x <= '9') return x - '0';
	if ('A' <= x && x <= 'Z') return x - 'A' + 10;
	if ('a' <= x && x <= 'z') return x - 'a' + 36;
	return -1;
}

template <size_t FILE_LIMIT>
class TrieNode {
private:
	bitset<FILE_LIMIT> mask;
	int nextID[CHAR_RANGE];	

public:

	TrieNode() {
		memset(nextID, 0, sizeof nextID);
	}
	// ~TrieNode();

	int &NextID(char chr) {
		return nextID[chr];
	}
	bitset<FILE_LIMIT> &Mask() {		
		return mask;
	}
};


template <size_t FILE_LIMIT>
class TrieTree {
private:
	vector< TrieNode<FILE_LIMIT> > listValue;

public:

	TrieTree() {
		listValue.clear();
		listValue.push_back(TrieNode<FILE_LIMIT>());
	}
	// ~TrieTree();

	//Insert a string to trie and a file ID which it's belong to
	void InsertString(const string &entry, int ID, int allSubString = 0) {
		for (int id = 0; id < entry.length(); id++) {
			int index = 0;
			for (int i = id; i < entry.length(); i++) {
				if (allSubString > 0)
					listValue[index].Mask()[ID] = true;
				if (!listValue[index].NextID(entry[i])) {
					listValue[index].NextID(entry[i]) = listValue.size();
					listValue.push_back(TrieNode<FILE_LIMIT>());
				}
				index = listValue[index].NextID(entry[i]);
			}
			listValue[index].Mask()[ID] = true;
			if (allSubString < 2) break;
		}
	}

	//Get the set of files (bitmask) that contain the word
	bitset<FILE_LIMIT> GetDocumentsMask(const string &entry) {
		int index = 0;
		for (int i = 0; i < entry.length(); i++) {
			if (!listValue[index].NextID(entry[i])) 
				return listValue[0].Mask();
			index = listValue[index].NextID(entry[i]);
		}
		return listValue[index].Mask();
	}
};