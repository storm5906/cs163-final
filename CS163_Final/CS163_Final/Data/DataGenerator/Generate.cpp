#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <functional>
#include <vector>

using namespace std;

int frequence[] = {1, 10, 100, 1000, 5000};
vector<string> words;
vector<char> sepChar;

template<typename T>
void Load(string filePath, vector<T> &a) {
    a.clear();
    ifstream reader(filePath);

    T x;
    while (reader >> x) a.push_back(x);
    reader.close();
}

int main() {

    Load("CommonWords.txt", words);
    Load("SeparateCharacters.txt", sepChar);
    sepChar.push_back(' ');
    sepChar.push_back('\n');
    sepChar.push_back('\t');

	srand(time(NULL));
	string filePath = "DATA_ .txt";
	for (char test = '0'; test < '5'; test++) {

		filePath[5] = test;
		ofstream writter(filePath);

		for (int word = 1; word <= 5000; word++) {
            int cSep = rand()%5 + 1;
            for (int i = 1; i <= cSep; i++) writter << sepChar[rand()%sepChar.size()];
            writter << words[rand()%frequence[test-'0']];
		}

        writter.close();
	}

	return 0;
}
