﻿/*
	Student 1 Name:   Nguyễn Lê Bảo     -	   1351051    -    nlbao@apcs.vn
	Student 2 Name:   Trương Minh Bảo   -	   1351052    -    tmbao@apcs.vn
	Course: CS163
	Program #: Final Project - Mini Search Engine
	Date: 27/06/2014
	File: files.h
*/

#include <stdio.h>
#include <string.h>


// read all content in a file to a character array,
// does not include EOF character,
// return length of the buffer
int bufferRead(FILE* &file, char* &buffer) {
	fseek(file, 0, SEEK_END);
	int fileSize = ftell(file);
	rewind(file);

	if (buffer) delete[] buffer;
	buffer = new char[fileSize+1];
	fread(buffer, 1, fileSize, file);

	int n = strlen(buffer);
	for (int i = 0; i < n; ++i)
		if (buffer[i] < 0) {
			buffer[i] = '\0';
			return i;
		}
	return n;
}