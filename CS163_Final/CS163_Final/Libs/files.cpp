﻿/*
	Student 1 Name:   Nguyễn Lê Bảo     -	   1351051    -    nlbao@apcs.vn
	Student 2 Name:   Trương Minh Bảo   -	   1351052    -    tmbao@apcs.vn
	Course: CS163
	Program #: Final Project - Mini Search Engine
	Date: 27/06/2014
	File: files.cpp
*/

#include "files.h"


int bufferRead(FILE* &file, char* &buffer) {
	fseek(file, 0, SEEK_END);
	int fileSize = ftell(file);
	rewind(file);

	char *tmp = new char[fileSize+1];
	fread(tmp, 1, fileSize, file);

	int n = strlen(tmp), m = 0;
	for (int i = 0; i < n; ++i)
		m += (tmp[i] >= 0);
	
	if (buffer) delete[] buffer;
	buffer = new char[m+1];
	for (int i = 0, c = 0; i < n; ++i)
		if (tmp[i] >= 0)
			buffer[c++] = tmp[i];
	buffer[m] = '\0';
	return m;
}
