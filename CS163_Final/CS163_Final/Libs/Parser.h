﻿/*
	Student 1 Name:   Nguyễn Lê Bảo     -	   1351051    -    nlbao@apcs.vn
	Student 2 Name:   Trương Minh Bảo   -	   1351052    -    tmbao@apcs.vn
	Course: CS163
	Program #: Final Project - Mini Search Engine
	Date: 27/06/2014
	File: Parser.h
*/
#define _CRT_SECURE_NO_WARNINGS 
#pragma once
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>

using namespace std;

#include "files.h"
#include "Trie.h"

#define WORD_LENGTH 65536

namespace DocumentParser {
	//Return false if throw error when opening file
	template<size_t FILE_LIMIT>
	bool ParseFromFile(const string &filePath, int fileID, TrieTree<FILE_LIMIT> &normalTrie, TrieTree<FILE_LIMIT> &preTrie, TrieTree<FILE_LIMIT> &sufTrie, TrieTree<FILE_LIMIT> &suffixTrie) {
		FILE* file = fopen(filePath.c_str(), "r");

		if (file) {
			/*Normal reading*/
			// char buff[WORD_LENGTH] = {0};
			// while (fscanf(file, "%[0-9a-zA-Z]", buff) != EOF) {
			// 	tree.InsertString(buff, fileID);
			// 	//printf("%s\n", buff);
			// 	fscanf(file, "%[^0-9a-zA-Z]", buff);
			// }


			/*Buffer reading*/
			char *buffer = NULL;
			int n = bufferRead(file, buffer);

			/*string delim = "";
			for (int x = -128; x < 128; x++)
				if (x && charToNum(char(x)) == -1)
					delim += char(x);

			char *pEntry = strtok(buffer, delim.c_str());
			while (pEntry) {
				string entry = pEntry;
				if (!entry.empty()) {
					normalTrie.InsertString(entry, fileID, 0);
					preTrie.InsertString(entry, fileID, 1);
					sufTrie.InsertString(string(entry.rbegin(), entry.rend()), fileID, 1);
					suffixTrie.InsertString(entry, fileID, 2);
				}
				pEntry = strtok(NULL, delim.c_str());
			}
			free(pEntry);*/

			for (int i = 0; i < n; ++i) {
				string entry = "";
				int j = i;
				while (j < n && charToNum(buffer[j]) != -1) {
					entry += buffer[j];
					++j;
				}
				if (!entry.empty()) {
					normalTrie.InsertString(entry, fileID, 0);
					preTrie.InsertString(entry, fileID, 1);
					sufTrie.InsertString(string(entry.rbegin(), entry.rend()), fileID, 1);
					suffixTrie.InsertString(entry, fileID, 2);
				}
				i = j;
			}

			free(buffer);

			fclose(file);
			return true;
		}

		return false;
	}
};
