﻿/*
	Student 1 Name:   Nguyễn Lê Bảo     -	   1351051    -    nlbao@apcs.vn
	Student 2 Name:   Trương Minh Bảo   -	   1351052    -    tmbao@apcs.vn
	Course: CS163
	Program #: Final Project - Mini Search Engine
	Date: 27/06/2014
	File: main.cpp
*/

#include "Libs\Parser.h"
#include "Libs\Query.h"
 #include <iostream>

using namespace std;

int nFile;
char s[QUERY_LENGTH];


int main(int argc, char *argv[]) {
	// create and update trieTree
	clock_t t1 = clock(), startTime = t1;
	FILE *file = fopen("data/d0000.txt", "r");
	fscanf(file, "%d", &nFile);
	fclose(file);

	printf("Parse data files....\n");
	
	const int t = *const_cast<int*>(&nFile);
	const size_t tmp = *const_cast<int*>(&nFile);
	TrieTree<FILE_LIMIT> normalTrie, preTrie, sufTrie, suffixTrie;
	string fileName;
	for (int i = 1; i <= nFile; ++i) {
		fileName = to_string(i);
		while (fileName.length() < 4)
			fileName = "0" + fileName;
		fileName = "d" + fileName + ".txt";
		printf("Read file: %s\n", fileName.c_str());
		DocumentParser::ParseFromFile("data/" + fileName, i, normalTrie, preTrie, sufTrie, suffixTrie);

		//-------increase number of test files---------//
		//if (i == nFile) {
		//	for (int k = 1; k <= 300; ++k)
		//	DocumentParser::ParseFromFile("data/" + fileName, i, trieTree);
		//}
		//--------------------------------------------//
	}
	printf("Data parsing runtime = %f ms\n", ((float)clock() - (float)t1)/CLOCKS_PER_SEC*1000);

	// process queries
	t1 = clock();
	printf("Process queries....\n");
	FILE *queryFile = fopen("data/query.txt", "r");
	if (!queryFile) {
		printf("Error when open query.txt file!\n");
		return -1;
	}
	FILE *outFile = fopen("output.txt", "w");

	/* Normal reading */
	//int n = 0;
	//string rawQuery;
	//fscanf(queryFile, "%d\n", &n);
	//for (int i = 0; i < n; ++i) {
	//	fgets(s, QUERY_LENGTH-1, queryFile);

	//	//printf("%s\n", s);
	//	rawQuery = string(s);
	//	Query query(rawQuery);
	//	query.WriteResult(outFile, trieTree);
	//}


	/* buffer reading */
	char *buffer = NULL;
	int len = bufferRead(queryFile, buffer), n = 0;
	for (int i = 0, foundN = 0; i < len; ++i)
		if (!foundN) {
			while (buffer[i] < '0' || buffer[i] > '9') ++i;
			while ('0' <= buffer[i] && buffer[i] <= '9')
				n = n*10 + buffer[i++] - '0';
			foundN = 1;
		}
		else {
			int j = i;
			while (j < len && buffer[j] != '\n') ++j;
			if (i < j) {
				Query<FILE_LIMIT> query(buffer, i, j-1, &normalTrie, &preTrie, &sufTrie, &suffixTrie);
				query.WriteResult(outFile);
				if (--n == 0) break;
			}
			i = j;
		}
	free(buffer);

	fclose(outFile);
	fclose(queryFile);
	printf("Queries processing runtime = %f ms\n", ((float)clock() - (float)t1)/CLOCKS_PER_SEC*1000);
	printf("Summary = %f ms\n", ((float)clock() - (float)startTime)/CLOCKS_PER_SEC*1000);
	printf("Done!\n");
	return 0;
}