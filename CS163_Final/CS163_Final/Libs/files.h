﻿/*
	Student 1 Name:   Nguyễn Lê Bảo     -	   1351051    -    nlbao@apcs.vn
	Student 2 Name:   Trương Minh Bảo   -	   1351052    -    tmbao@apcs.vn
	Course: CS163
	Program #: Final Project - Mini Search Engine
	Date: 27/06/2014
	File: files.h
*/

#include <stdio.h>
#include <string.h>


// read all content in a file to a character array,
// does not include EOF character,
// return length of the buffer
int bufferRead(FILE* &file, char* &buffer);