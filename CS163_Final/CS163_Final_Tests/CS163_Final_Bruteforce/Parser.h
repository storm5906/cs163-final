﻿/*
	Student 1 Name:   Nguyễn Lê Bảo     -	   1351051    -    nlbao@apcs.vn
	Student 2 Name:   Trương Minh Bảo   -	   1351052    -    tmbao@apcs.vn
	Course: CS163
	Program #: Final Project - Mini Search Engine
	Date: 27/06/2014
	File: Parser.h
*/
#define _CRT_SECURE_NO_WARNINGS 
#pragma once
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include "files.h"

using namespace std;

#include "Trie.h"

#define WORD_LENGTH 65536

namespace DocumentParser {
	//Return false if throw error when opening file
	template<size_t FILE_LIMIT>
	bool ParseFromFile(const string &filePath, int fileID, TrieTree<FILE_LIMIT> &tree) {
		FILE* file = fopen(filePath.c_str(), "r");

		if (file) {
			/*Normal reading*/
			// char buff[WORD_LENGTH] = {0};
			// while (fscanf(file, "%[0-9a-zA-Z]", buff) != EOF) {
			// 	tree.InsertString(buff, fileID);
			// 	//printf("%s\n", buff);
			// 	fscanf(file, "%[^0-9a-zA-Z]", buff);
			// }


			/*Buffer reading*/
			char *buffer = NULL;
			int n = bufferRead(file, buffer);

			/*char *entry = strtok(buffer, "`~!@#$%^&*()-_=+\t\\|[]{};:'\"\r\n,<.>/? ");
			while (entry) {
				cout << entry << endl;
				cout << "length = " << strlen(entry) << endl;
				if (*entry) tree.InsertString(entry, fileID);
				entry = strtok(NULL, "`~!@#$%^&*()-_=+\t\\|[]{};:'\"\r\n,<.>/? ");
			}*/

			for (int i = 0; i < n; ++i) {
				int j = i;
				while (j < n && charToNum(buffer[j]) != -1)
					++j;
				tree.InsertString(buffer, i, j-1, fileID);
				i = j;
			}
			free(buffer);

			fclose(file);
			return true;
		}

		return false;
	}
};
