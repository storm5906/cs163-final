﻿/*
	Student 1 Name:   Nguyễn Lê Bảo     -	   1351051    -    nlbao@apcs.vn
	Student 2 Name:   Trương Minh Bảo   -	   1351052    -    tmbao@apcs.vn
	Course: CS163
	Program #: Final Project - Mini Search Engine
	Date: 27/06/2014
	File: Query.h
*/

#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <time.h>

using namespace std;

#include "Trie.h"

const int QUERY_LENGTH = int(1e5);
const int WORD_SEPARATOR_SIZE = 4;
const char WORD_SEPARATOR[] = {' ', '\r', '\n', '\t'};


// contains words and operators between words
template<size_t FILE_LIMIT>
class Query {
public:
	// constructor with a raw query string 
	Query(const char rawQuery[], int begin, int end, TrieTree<FILE_LIMIT> *normalTrie, TrieTree<FILE_LIMIT> *preTrie, TrieTree<FILE_LIMIT> *sufTrie, TrieTree<FILE_LIMIT> *suffixTrie);

	// output query to the screen
	void Output() const;

	// get list of bitset of files
	// if bit[i] == true then the i-th file satisfied the query
	// return run time of query
	int Find(bitset<FILE_LIMIT> &mask) const;

	// write result a file
	void WriteResult(FILE* &outFile) const;


private:
	// words and operators in the query
	vector<string> _words;
	vector<int> _operAndIndex;
	vector<int> _operOrIndex;
	TrieTree<FILE_LIMIT> *_normalTrie, *_preTrie, *_sufTrie, *_suffixTrie;
};


/*--------------------Other functions--------------------*/

// check if a character is a word-separator
bool isWordSeparator(char x);



/*------------------------IMPLEMENT------------------------*/

// constructor with a raw query string
template<size_t FILE_LIMIT> Query<FILE_LIMIT>::Query(const char rawQuery[],
													 int begin, int end, 
													 TrieTree<FILE_LIMIT> *normalTrie, 
													 TrieTree<FILE_LIMIT> *preTrie, 
													 TrieTree<FILE_LIMIT> *sufTrie, 
													 TrieTree<FILE_LIMIT> *suffixTrie)
{
	if (end < begin)
		return;

	_normalTrie = normalTrie;
	_preTrie = preTrie;
	_sufTrie = sufTrie;
	_suffixTrie = suffixTrie;

	string s;
	int wordCount = -1;
	for (int i = begin, j = begin; i <= end; i = j) {
		// get a word
		s = "";
		while (j <= end && !isWordSeparator(rawQuery[j]))
			s += rawQuery[j++];
		// len(s) == 0
		if (j == i)	{	
			++j;
			continue;
		}
		// len(s) > 0
		if (s == "AND")
			this->_operAndIndex.push_back(wordCount);
		else if (s == "OR")
			this->_operOrIndex.push_back(wordCount);
		else {
			this->_words.push_back(s);
			++wordCount;
		}
	}
}


// output query to the screen
template<size_t FILE_LIMIT> void Query<FILE_LIMIT>::Output() const {
	cout << "WORDS: ";
	for (int i = 0, sz = this->_words.size(); i < sz; ++i)
		cout << this->_words[i] << " ";
	cout << endl << "AND: ";
	for (int i = 0, sz = this->_operAndIndex.size(); i < sz; ++i)
		cout << this->_operAndIndex[i] << " ";
	cout << endl << "OR: ";
	for (int i = 0, sz = this->_operOrIndex.size(); i < sz; ++i)
		cout << this->_operOrIndex[i] << " ";
	cout << endl;
}


// get list of bitset of files
// if bit[i] == true then the i-th file satisfied the query
// return run time of query
template<size_t FILE_LIMIT> int Query<FILE_LIMIT>::Find(bitset<FILE_LIMIT> &mask) const {
	clock_t start_time = clock();
	// init
	int nWord = this->_words.size();
	int *id = new int[nWord];
	bitset<FILE_LIMIT> *ans = new bitset<FILE_LIMIT>[nWord];
	for (int i = 0; i < nWord; ++i) {
		id[i] = i;
		if (_words[i].front() != '*' && _words[i].back() != '*')
			ans[i] = _normalTrie->GetDocumentsMask(_words[i]);
		else if (_words[i].front() == '*' && _words[i].back() == '*')
			ans[i] = _suffixTrie->GetDocumentsMask(string(_words[i].begin() + 1, _words[i].end() - 1));
		else if (_words[i].front() == '*')
			ans[i] = _sufTrie->GetDocumentsMask(string(_words[i].begin() + 1, _words[i].end()));
		else // (_words[i].back() == '*')
			ans[i] = _preTrie->GetDocumentsMask(string(_words[i].begin(), _words[i].end() - 1));
	}
	// AND operators
	for (int i = 0, nAnd = this->_operAndIndex.size(); i < nAnd; ++i) {
		int pos = this->_operAndIndex[i];
		int x = id[pos], y = id[pos+1];
		ans[x] = ans[x] & ans[y];
		id[y] = x;
	}
	// OR operators
	for (int i = 0, nOr = this->_operOrIndex.size(); i < nOr; ++i) {
		int pos = this->_operOrIndex[i];
		int x = id[pos], y = id[pos+1];
		ans[x] = ans[x] | ans[y];
		id[y] = x;
	}
	// result
	mask = ans[0];
	delete[] id;
	delete[] ans;
	return int(((float)clock() - (float)start_time)/CLOCKS_PER_SEC*1000);
}


// write result a file
template<size_t FILE_LIMIT> void Query<FILE_LIMIT>::WriteResult(FILE* &outFile) const {
	bitset<FILE_LIMIT> mask;
	int runTime = Find(mask);
	fprintf(outFile, "%d", runTime);
	string s;
	for (int i = 0; i < FILE_LIMIT; ++i)
		if (mask[i] == true) {
			s = to_string(i);
			while (s.length() < 4) s = "0" + s;
			s = "d" + s;
			fprintf(outFile, " %s", s.c_str());
		}
	fprintf(outFile, "\n");
}


bool isWordSeparator(char x) {
	for (int i = 0; i < WORD_SEPARATOR_SIZE; ++i)
		if (x == WORD_SEPARATOR[i])
			return true;
	return false;
}