[APCS] CS163 Final Project - Mini Search Engine
=====================
----------


## Authors:
* Bao Nguyen Le
* Bao Truong Minh


## Project description:
* Designing and  implementing a  mini search engine.
* Given a set of texts and a query, the search engine will locate all documents that contain the keywords in the query.
* The queries may contain simple Boolean operators, that  is  “AND”  and  “OR”, which act in  a  similar  manner  with  the  well-known  analogous  logical  operators.


## Extra credit
* Document scoring with respect to the query (i.e. keep track of the number of
occurrences of words in documents, and calculate a score based on that). If you
plan to implement this part, please see me for additional details.
* Incomplete matches. That is, allow for a search for number*, which will match
all words starting with number, i.e. number, numbers, numbering, etc.
* Stop words elimination. Given a list of very frequent words, like the, a, of, etc.,
you may consider avoiding the storage of these elements.