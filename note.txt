+ Problems:
	+ You have a set of text file.
	+ Let user enter a query (word) to search, then output all file which have that word.
	+ Some extra function.

+ Input files:
	d0000.txt 	--> an integer: number of input file (ex: n)
	d0001.txt
	.........
	dn.txt
	+ Each file contains many words (n = ???? words)
	+ WORD_LENGTH ???

+ Load multi files at same time.
+ Queries file: query.txt:
	+ n: number of queries
	+ next n lines:
		+ i-th line: i-th query

+ Output lines:
	+ 1-st line: runtime (ms)
	+ next lines:
		+ i-th line: runtime (ms), i-th query result
			+ Ex: 500 d0001 d0002
	+ output time is integer or double?
	+ only d.... or data.... or ???


+ Improve speed when read file (main.cpp)


(a AND b) OR (c AND d) = OR AND a b AND c d

(1+2)*(3+4) = * + 1 2 + 3 4