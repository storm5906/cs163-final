﻿/*
	Student 1 Name:   Nguyễn Lê Bảo     -	   1351051    -    nlbao@apcs.vn
	Student 2 Name:   Trương Minh Bảo   -	   1351052    -    tmbao@apcs.vn
	Course: CS163
	Program #: Final Project - Mini Search Engine
	Date: 27/06/2014
	File: Trie.h
*/

#pragma once
#include <cstring>
#include <bitset>
#include <map>

using namespace std;

#define CHAR_RANGE 128 //The range of characters in decimal


// convert a character to an integer, use for Trie indexing
int charToNum(char x) {
	if ('0' <= x && x <= '9') return x - '0';
	if ('A' <= x && x <= 'Z') return x - 'A' + 10;
	if ('a' <= x && x <= 'z') return x - 'a' + 36;
	return -1;
}


template <size_t FILE_LIMIT>
class TrieNode {
private:
	bitset<FILE_LIMIT> mask;
	int nextID[CHAR_RANGE];	

public:

	TrieNode() {
		memset(nextID, 0, sizeof nextID);
	}
	// ~TrieNode();

	int &NextID(char chr) {
		return nextID[chr];
	}
	bitset<FILE_LIMIT> &Mask() {		
		return mask;
	}
};


template <size_t FILE_LIMIT>
class TrieTree {
private:
	map< string, bitset<FILE_LIMIT> > listValue;

public:

	TrieTree() {
		listValue.clear();
	}
	// ~TrieTree();

	//Insert a string to trie and a file ID which it's belong to
	void InsertString(const char entry[], int begin, int end, int ID) {
		string sEntry = "";
		for (int i = begin; i <= end; i++) 
			sEntry += entry[i];
		listValue[entry][ID] = 1;		
	}

	//Get the set of files (bitmask) that contain the word
	bitset<FILE_LIMIT> GetDocumentsMask(const char entry[]) {
		return listValue[entry];
	}
};